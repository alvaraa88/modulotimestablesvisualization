# Modulo Times Table

Hello, Thanks for checking out this cool little project out.
It was inspired by one of my classes at UNM.
Here are some of the main points I would like to express as the objective of this project.
1) Mathmatical Visualization (Processing)
2) GUI implementation.
3) Unit testing

## Getting started:
### jar:
There are currently 3 main buttons used for animation.
1) Animation Multiplier 
   1) You can see continous patterns emerging from a multiplier starting from  (2.0 to infinity)
   2) 0 and 1 will be blank as the equation does not account for 0 and/or 1.
2) Animation Points
   1) You can see continous patterns emerging from points starting from (2 to 360) points as the points grow larger.. well... You'll see.
3) Show a Pattern
   1) Based on the multiplier and points you will see a pattern."
4) **note** 
   1) speed at which the animation runs is constant. it cannot be changed at the moment.

### Project files:
* MTT consists of 2 distict Folders. resources and source file
> **resources** is responsible for the GUI "view". It will show you the details of each element 
> loaded for the graphical user interface. Each fxml is connected to its Controller model. Which is
> responsible for the actions of each element in the GUI.
1) .fxml
    1) Must reference a specified Controller class.
   
> **src** is the backend code. 
> The backend code consists of a couple of classes:
1) **FXMLController** 
    1) class is a helper class for loading pages.
2) **StartPageController** 
   1) is the start page of the GUI with instructions on how to use the interface on the next page.
3) **TimesTablesMainController** 
   1) is where all the code for the Modulo Times Table goes.

### Author and website:
1) [Alexander Alvara](https://alexanderalvara.com/resume)
2) [Gitlab](https://gitlab.com/alvaraa88/modulotimestablesvisualization)
3) [LinkedIn](https://www.linkedin.com/in/alexander-alvara-4132a3a7/)
