package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import java.net.URL;
import java.util.ResourceBundle;

public class StartPageController implements Initializable, iSetPage {

    FXMLController myController;
    @FXML
    Button buttonStart = new Button();
    @FXML
    Pane paneStart = new Pane();
    @FXML
    Label labelMTTV = new Label();
    @Override
    public void setPageParent(FXMLController setPage) {
        myController = setPage;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    /**
     * this is where the ID's are named to call the FXML files.
     */
    @FXML
    private void goToTimesPage(ActionEvent event) {
        myController.setPage("times");
        //could reset value of TimesTables
    }

}
