package controller;

import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * This class directs the pages.
 */
public class TimesTablesMainController implements Initializable, iSetPage {
    /**
     * -----------------
     * FXML variables
     * -------------------
     */
    FXMLController myController;
    @FXML
    Pane paneTimes;
    @FXML
    Button buttonShow;
    @FXML
    Button buttonStartAnimationPoint;
    @FXML
    Button buttonStartAnimationMult;
    @FXML
    Button buttonReset;
    @FXML
    Button buttonStartPage;
    @FXML
    Label labelNumberOfPoints;
    @FXML
    Label labelStartVisualization;
    @FXML
    Label labelSpeedAnimation;
    @FXML
    Label labelTTMultiplier;
    @FXML
    Label labelFavorites;
    @FXML
    TextField textFieldSpeedAnimation;
    @FXML
    TextField textFieldNumberOfPoints;
    @FXML
    TextField textFieldTTMultiplier;
    @FXML
    Hyperlink hLink1;
    @FXML
    Hyperlink hLink2;
    @FXML
    Hyperlink hLink3;
    @FXML
    Hyperlink hLink4;

    /*---------------
    local variables
     ---------------*/
    Circle point;
    Line line;
    int numberOfPointsValue;
    int speedOfAnimationValue;
    Double TTMultiplier;
    ArrayList<Circle> points = new ArrayList<>();
    ArrayList<Line> lines = new ArrayList<>();
    int mod;
    boolean checkGUI = false;

    Timeline timelineMult, timelinePoints;

    //Timeline timelineMain;
    /*---------------
    Timeline ==> for animation on pane
     ----------------*/



    @Override
    public void setPageParent(FXMLController setPage) {
        myController = setPage;
    }

    // note: hierarchy of loaded objects --> 1)constructor 2)fxml  3)initialize
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        textFieldNumberOfPoints.setText("360");
        textFieldSpeedAnimation.setText("200");
        textFieldTTMultiplier.setText("2.0");
        buttonReset.setDisable(true);
    }



    public void createPoints(int numberOfPoints) {

        int centerX = 400;
        int centerY = 200;
        int circleRadius = 1; //size of points
        int pointRadius = 150; // this is the size of a circle full of points.

        for (int i = 0; i < numberOfPoints; i++) {
            //360 represents the unit circle from left to right (instead of right to left)
            //(numberOfPoints) represents the number of points that will occur
            // i represents the number of po
            double inDegrees = 360 * i / (numberOfPoints);
            double inRadians = Math.toRadians(inDegrees);
            double angle = inRadians;
            double xPosOffset = centerX + (Math.cos(angle) * pointRadius);
            double yPosOffset = centerY + (Math.sin(angle) * pointRadius);

            point = new Circle();
            point.setTranslateX(xPosOffset);
            point.setTranslateY(yPosOffset);
            point.setFill(Color.RED);
            point.setRadius(circleRadius);
            points.add(point);

            paneTimes.getChildren().add(points.get(i));
        }
    }

    public void removePointsFromGUI(int numberOfPoints) {
        for (int i = 0; i < numberOfPoints; i++) {
            paneTimes.getChildren().remove(points.get(i));
        }
        points.removeAll(points);
    }

    public void createPointConnections() {
        for (int i = 0; i < numberOfPointsValue; i++) {
            int result = (TTMultiplier.intValue() * i) % numberOfPointsValue;
            line = new Line(
                    points.get(i).getTranslateX(),
                    points.get(i).getTranslateY(),
                    points.get(result).getTranslateX(),
                    points.get(result).getTranslateY());
            line.setStroke(Color.BLACK);
            lines.add(line);

            paneTimes.getChildren().add(lines.get(i));

        }
    }

    public void removeLinesFromGUI(int numberOfPoints) {
        for (int i = 0; i < numberOfPoints; i++) {
            paneTimes.getChildren().remove(lines.get(i));
        }
        lines.removeAll(lines);
    }
    public void removeGUI(int numPoints){
        removeLinesFromGUI(numPoints);
        removePointsFromGUI(numPoints);
    }

    //for testing values of line connections--> working correctly
    public void multiplicationTable(Double multiplier, int numberOfPoints) {
        mod = numberOfPoints;
        for (int i = 0; i <= numberOfPoints; ++i) {
            System.out.printf("%f * %d = %d \n", multiplier, i, (multiplier.intValue() * i) % mod);
        }
    }

    /**
     * this is where the ID's are named to call the FXML files.
     *
     * @param event
     */
    @FXML
    private void goToStartPage(ActionEvent event) {
        myController.setPage("startPage");
    }

    /**
     * Methods
     */
    @FXML
    public void startVisualization() {
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());
        mod = Integer.valueOf(textFieldNumberOfPoints.getText());

        if(checkGUI == true){
            removeGUI(numberOfPointsValue);
        }

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();
        checkGUI = true;
        textFieldNumberOfPoints.setDisable(true);
        buttonStartAnimationMult.setDisable(true);
        buttonStartAnimationPoint.setDisable(true);
        buttonReset.setDisable(false);
    }

    @FXML
    public void startAnimationMultiplier() {
        System.out.println("start multiplier animation");
        buttonStartAnimationPoint.setDisable(true);
        buttonShow.setDisable(true);
        buttonReset.setDisable(false);
        buttonStartAnimationMult.setDisable(false);
        textFieldTTMultiplier.setText("2.0");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());
        textFieldTTMultiplier.setText(TTMultiplier.toString());
        textFieldSpeedAnimation.setText(String.valueOf(speedOfAnimationValue));
        textFieldNumberOfPoints.setText(String.valueOf(numberOfPointsValue));
        mod = Integer.valueOf(textFieldNumberOfPoints.getText());

        textFieldNumberOfPoints.setDisable(true);
        textFieldSpeedAnimation.setDisable(true);
        textFieldTTMultiplier.setDisable(true);

        hLink1.setDisable(true);
        hLink2.setDisable(true);
        hLink3.setDisable(true);
        hLink4.setDisable(true);

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();

        timelineMult = new Timeline(new KeyFrame(Duration.millis(speedOfAnimationValue), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TTMultiplier += 1;
                textFieldTTMultiplier.setText(String.valueOf(TTMultiplier));
                removeLinesFromGUI(numberOfPointsValue);
                createPointConnections();
                //checkGUI = true;
            }
        }));
        timelineMult.setCycleCount(Animation.INDEFINITE);
        timelineMult.play();
    }

    @FXML
    public void startAnimationPoints() {
        System.out.println("start point animation");
        buttonStartAnimationMult.setDisable(true);
        buttonShow.setDisable(true);
        buttonReset.setDisable(false);
        buttonStartAnimationPoint.setDisable(false);
        textFieldNumberOfPoints.setDisable(true);
        textFieldSpeedAnimation.setDisable(true);
        textFieldNumberOfPoints.setText("2");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());
        textFieldTTMultiplier.setText(TTMultiplier.toString());
        textFieldSpeedAnimation.setText(String.valueOf(speedOfAnimationValue));
        textFieldNumberOfPoints.setText(String.valueOf(numberOfPointsValue));
        mod = Integer.valueOf(textFieldNumberOfPoints.getText());

        textFieldTTMultiplier.setDisable(true);
        textFieldNumberOfPoints.setDisable(true);
        textFieldSpeedAnimation.setDisable(true);

        hLink1.setDisable(true);
        hLink2.setDisable(true);
        hLink3.setDisable(true);
        hLink4.setDisable(true);



        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();

        timelinePoints = new Timeline(new KeyFrame(Duration.millis(speedOfAnimationValue), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                removePointsFromGUI(numberOfPointsValue);
                removeLinesFromGUI(numberOfPointsValue);
                numberOfPointsValue += 1;
                textFieldNumberOfPoints.setText(String.valueOf(numberOfPointsValue));
                createPoints(numberOfPointsValue);
                createPointConnections();
                //checkGUI = true;
            }
        }));
        timelinePoints.setCycleCount(Animation.INDEFINITE);
        timelinePoints.play();
    }


    @FXML
    public void resetVisualization() {
        System.out.println("reset");

        if(timelineMult != null) {
            timelineMult.stop();
        }
        if(timelinePoints != null){
            timelinePoints.stop();
        }
        textFieldTTMultiplier.setText("2.0");
        textFieldSpeedAnimation.setText("100");
        textFieldNumberOfPoints.setText("360");
        textFieldNumberOfPoints.setDisable(false);
        buttonStartAnimationMult.setDisable(false);
        buttonStartAnimationPoint.setDisable(false);
        buttonShow.setDisable(false);
        buttonReset.setDisable(true);
        textFieldNumberOfPoints.setDisable(false);
        textFieldSpeedAnimation.setDisable(false);
        textFieldTTMultiplier.setDisable(false);

        hLink1.setDisable(false);
        hLink2.setDisable(false);
        hLink3.setDisable(false);
        hLink4.setDisable(false);
        removeGUI(numberOfPointsValue);
        checkGUI = false;
    }

    @FXML
    public void favoritePattern1(){
        textFieldTTMultiplier.setText("4");
        textFieldSpeedAnimation.setText("100");
        textFieldNumberOfPoints.setText("360");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());

        if(checkGUI == true){
            removeGUI(numberOfPointsValue);
        }

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();
        checkGUI = true;
        textFieldNumberOfPoints.setDisable(true);
        buttonStartAnimationMult.setDisable(true);
        buttonStartAnimationPoint.setDisable(true);
        buttonReset.setDisable(false);
    }
    @FXML
    public void favoritePattern2(){
        textFieldTTMultiplier.setText("8");
        textFieldSpeedAnimation.setText("100");
        textFieldNumberOfPoints.setText("360");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());

        if(checkGUI == true){
            removeGUI(numberOfPointsValue);
        }

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();
        checkGUI = true;
        textFieldNumberOfPoints.setDisable(true);
        buttonStartAnimationMult.setDisable(true);
        buttonStartAnimationPoint.setDisable(true);
        buttonReset.setDisable(false);
    }
    @FXML
    public void favoritePattern3(){
        textFieldTTMultiplier.setText("49");
        textFieldSpeedAnimation.setText("100");
        textFieldNumberOfPoints.setText("360");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());

        if(checkGUI == true){
            removeGUI(numberOfPointsValue);
        }

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();
        checkGUI = true;
        textFieldNumberOfPoints.setDisable(true);
        buttonStartAnimationMult.setDisable(true);
        buttonStartAnimationPoint.setDisable(true);
        buttonReset.setDisable(false);

    }
    @FXML
    public void favoritePattern4(){
        textFieldTTMultiplier.setText("250");
        textFieldSpeedAnimation.setText("100");
        textFieldNumberOfPoints.setText("360");
        numberOfPointsValue = Integer.valueOf(textFieldNumberOfPoints.getText());
        speedOfAnimationValue = Integer.valueOf(textFieldSpeedAnimation.getText());
        TTMultiplier = Double.valueOf(textFieldTTMultiplier.getText());

        if(checkGUI == true){
            removeGUI(numberOfPointsValue);
        }

        //set points on pane.
        createPoints(numberOfPointsValue);
        createPointConnections();
        checkGUI = true;
        textFieldNumberOfPoints.setDisable(true);
        buttonStartAnimationMult.setDisable(true);
        buttonStartAnimationPoint.setDisable(true);
        buttonReset.setDisable(false);

    }


}
