package controller;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import java.util.HashMap;

/*
this class was inspired from java's youtube channel.
"Managing multiple screens in JavaFX".
https://www.youtube.com/watch?v=5GsdaZWDcdY&t=523s
 */


public class FXMLController extends StackPane {

    public FXMLController() {
        super();
    }

    //hold a collection of pages
    private final HashMap<String, Node> pages = new HashMap<>();

    //add the screen to Hashmap.
    public void addPage(String name, Node screen) {
        pages.put(name, screen);
    }

    //Returns the node with the appropriate name
    public Node getPage(String name) {
        return pages.get(name);
    }

    Parent loadScreen;

    /* loads fxml file, add the page to the hashmap and
    finally injects the page to the controller.
     */
    public boolean loadPage(String name, String resource) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(resource));
            loadScreen = (fxmlLoader.load());
            iSetPage myPageController = (fxmlLoader.getController());
            myPageController.setPageParent(this);
            addPage(name, loadScreen);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /*
    This method tries to display the page with a predefined name.
    first it makes sure the page has been already loaded. Then if there is more than one page the new page
    will be added second, and then the current page is removed.
    if there isn't any page being displayed, the new page is added to the root.
     */
    public boolean setPage(final String name) {
        if (pages.get(name) != null) {
            final DoubleProperty opacity = opacityProperty();

            //transition between pages
            if (!getChildren().isEmpty()) {
                Timeline fadeOut = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {

                            @Override
                            public void handle(ActionEvent t) {
                                getChildren().remove(0);
                                getChildren().add(0, pages.get(name));
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));
                fadeOut.play();
                //if there is no pages added default screen will display
            } else {
                setOpacity(0.0);
                getChildren().add(pages.get(name));
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(1000), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {
            System.out.println("screen hasn't been loaded!\n");
            return false;
        }
    }

    //This method will remove the page with the given name from the hashmap
    public boolean unloadPage(String name) {
        if (pages.remove(name) == null) {
            System.out.println("screen didn't exist");
            return false;
        } else {
            return true;
        }
    }
}
