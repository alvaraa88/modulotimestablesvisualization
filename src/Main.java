/**
 * Alexander Alvara
 * Modulo Times Table Project.
 */

import controller.FXMLController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class is responsible for running the maze.
 */
public class Main extends Application {

    public static String pageTimesMainID = "times";
    public static String pageTimesMainFXML = "times.fxml";
    public static String pageStartID = "startPage";
    public static String pageStartFXML = "startPage.fxml";

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLController mainViewContainer = new FXMLController();
        mainViewContainer.loadPage(Main.pageTimesMainID, Main.pageTimesMainFXML);
        mainViewContainer.loadPage(Main.pageStartID, Main.pageStartFXML);

        mainViewContainer.setPage(Main.pageStartID);

        Group root = new Group();
        root.getChildren().addAll(mainViewContainer);
        Scene scene = new Scene(root);
        primaryStage.setTitle("Times Table Visualization");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
